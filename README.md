### What is this repository for? ###

* PowerShell script to download NuGet Packages for offline use.
* Version 1.0

### How do I get set up? ###

* Download and install NuGet CLI from https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
* Include it in your PATH
* Configure parameters and set up directories
* Execute script ( permissions may have to be granted )