﻿# ╔═════════════════════════════════════════════════════════════════════╗
# ║Powershell Script GetPackages.ps1					║
# ║Version 1.0								║
# ║(C) Stephan Filipczyk						║
# ║									║
# ║Download NuGet Packages, install them temporarily , copy the package ║
# ║  files to a local repository and remove temporary data		║
# ║									║
# ║Note: nuget CLI  must be installed!					║
# ║ from  https://dist.nuget.org/win-x86-commandline/latest/nuget.exe	║
# ║									║
# ║Use at own risk!							║
# ╚═════════════════════════════════════════════════════════════════════╝

# ┌───────────┐
# │ Paramters │
# └───────────┘

# Temporary download / install target
$tempdl = 'D:\TEMPNUGET'

# Repository (Files will be overwritten if they exist)
$target = 'D:\NUGETLOCALREPO'

# Packages to fetch (will also fetch dependencies!)
$packages = @("log4net")

# ╔══════════════════════════════════╗
# ║ DO NOT CHANGE BELOW THIS LINE !! ║
# ╚══════════════════════════════════╝

cd $tempdl
foreach( $pack in $packages ){
    nuget install $pack
}

Copy-Item "$tempdl\*\*.nupkg" "$target" -Recurse -Verbose
Remove-Item -Path "$tempdl\*" -Recurse -Verbose

Write-Host "Press any key to continue ..."
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")